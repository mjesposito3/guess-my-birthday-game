from random import randint
user_name = ""
num_guesses = 5
number_month = randint(1, 12)
random_year= randint(1924, 2004)
user_response = ""
guess_num=1

#Ask for user name
user_name = input("Hello, what is your name? ")

#Guess Loop, asking random month/year
# while guess_num <5:


for num in range(num_guesses):
    print("Guess ", guess_num, ":" , user_name, " were you born in ",  number_month, "/", random_year, "?")
    user_response = input("yes or no? ")
    if user_response != "yes" or user_response != "no":
        print("invalid response, try again:")
    elif user_response == "yes":
        print("I knew it!")
        exit()
    elif num < num_guesses-1:
        print("Drat! Lemme try again!")
        guess_num +=1
        number_month = randint(1, 12)
        random_year= randint(1924, 2004)
        continue
    else:
        print("I have other things to do. Good bye.")
        exit()

